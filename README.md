## Completed plugin location
Currently it can be found in the `plugins` folder.

## Rev'ing build number 
Modify the following line and run ./gradlew shadowJar

../dashboards/hpe-home-dashboard-plugin/build.gradle
```
line 24: version '0.0.3'
```

## Files that were changed

### ../hpe-home-dashboard-plugin/src/main/groovy/com/morpheusdata/dashboard/MorpheusHomeDashboardPlugin.groovy

Modified this line for HPE:
```
line 17: 		return 'hpe-home-dashboard-plugin'
```

Commented out the following lines:
```
line 37:  // InstanceCountCloudDayItemProvider instanceCountCloudDayProvider = new InstanceCountCloudDayItemProvider(this, morpheus)
line 38:  // this.pluginProviders.put(instanceCountCloudDayProvider.code, instanceCountCloudDayProvider)

line 53:  // ClusterWorkloadCountItemProvider clusterWorkloadCountProvider = new ClusterWorkloadCountItemProvider(this, morpheus)
line 54:  // this.pluginProviders.put(clusterWorkloadCountProvider.code, clusterWorkloadCountProvider)
line 55:  // ClusterTypeCountItemProvider clusterTypeCountProvider = new ClusterTypeCountItemProvider(this, morpheus)
line 56:  // this.pluginProviders.put(clusterTypeCountProvider.code, clusterTypeCountProvider)
line 57:  // ClusterCapacityItemProvider clusterCapacityProvider = new ClusterCapacityItemProvider(this, morpheus)
line 58:  // this.pluginProviders.put(clusterCapacityProvider.code, clusterCapacityProvider)
```

```
Modified this line for HPE:
line 91:  //log.error("error initializing hpe home dashboard plugin: ${e}", e)
```


### ../hpe-home-dashboard-plugin/src/main/groovy/com/morpheusdata/dashboard/HomeDashboardProvider.groovy

Commented out the following lines:
```
line 80:  // 'dashboard-item-instance-count-cloud-day'
line 85:  // 'dashboard-item-cluster-workload-count'
```

### VSCODE DevContainer file: 

devcontainer.json

```
// For format details, see https://aka.ms/devcontainer.json. For config options, see the
// README at: https://github.com/devcontainers/templates/tree/main/src/ubuntu
{
	"name": "UbuntuDashboards01",
	// Or use a Dockerfile or Docker Compose file. More info: https://containers.dev/guide/dockerfile
	"image": "mcr.microsoft.com/devcontainers/base:jammy",


	"features": {
		"ghcr.io/devcontainers/features/java:1": {
			"version": "11",
			"installGradle": true,
			"installMaven": false,
			"gradleVersion": "6.5.1"
		}
	}
	// Features to add to the dev container. More info: https://containers.dev/features.
	// "features": {},

	// Use 'forwardPorts' to make a list of ports inside the container available locally.
	// "forwardPorts": [],

	// Use 'postCreateCommand' to run commands after the container is created.
	// "postCreateCommand": "uname -a",

	// Configure tool-specific properties.
	// "customizations": {},

	// Uncomment to connect as root instead. More info: https://aka.ms/dev-containers-non-root.
	// "remoteUser": "root"
}
```